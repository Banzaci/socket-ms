var NAME_OF_CACHE = 'cache-v1';
var filesToCache = [
  '/',
  '/stylesheets/main.min.css',
  '/javascripts/main.min.js'
];

self.addEventListener('install', function(event) {
    event.waitUntil(caches.open(NAME_OF_CACHE).then(function(cache) {
          console.log('Opened cache');
          return cache.addAll(urlsToCache);
    }));
});
self.addEventListener('fetch', function(event) {
  event.respondWith(caches.match(event.request).then(function(response) {
        if (response) {
            return response;
        }
        return fetch(event.request);
  }));
});
