(function(e){
    'use strict';
    e.CORE.subscribe('readMessage', (function( sandbox ){

        var arr = [];

        var responseSocket = function(response){
            var self = this;
            var container = self.container;
            var readMessage = container.child('js-read-message');
            var msNo = response.message.sort(function(a, b){
                  return a < b;
            });
            arr.push(msNo);

            var df = sandbox.element(document.createDocumentFragment());
            arr.forEach(function(res){
                df.createElement('li').html(res[0] + ' ' + res[1]);
            });
            readMessage.html('').append(df);
        };

        return {
            init:function(){
                sandbox.listen('newMessage', responseSocket.bind(this));
                //sandbox.service('serviceWorkers').setUp();
            },
            destroy:function(){}
        };

    }));
}( BEER ));
