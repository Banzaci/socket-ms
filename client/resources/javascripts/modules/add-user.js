(function(e){
    'use strict';
    e.CORE.subscribe('addUser', (function( sandbox ){
        var url = lib.URLparse('http://site.com:81/path/page?a=1&b=2#hash');
        var responseDB = function(response){
            sandbox.service('indexedDB').get('users', function(response){
                sandbox.notify('newUser', response);
            });
        };

        return {
            init:function(){

                var self = this;
                var container = self.container;
                var eventName = 'users';
                console.log(url);
                container.child('js-add-user').on('click', function(e){
                    e.preventDefault();
                    var age = Math.floor(Math.random() * 100) + 18;
                    var name = container.child('js-user-input');
                    var data = { name:name.value(), email:name.value()+'@somemail.com', age:age };
                    sandbox.service('indexedDB').save(eventName, data, responseDB.bind(self));
                    name.value('');
                });
            },
            destroy:function(){}
        };

    }));
}( BEER ));
