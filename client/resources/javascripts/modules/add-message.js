(function(e){
    'use strict';
    e.CORE.subscribe('addMessage', (function( sandbox ){

        var responseSocket = function(response){
            sandbox.notify('newMessage', response);
        };

        return {
            init:function(){
                var self = this;
                var container = self.container;
                var eventName = 'message';
                container.child('js-add-message').on('click', function(e){
                    e.preventDefault();
                    var message = container.child('js-message-input');
                    sandbox.service('socket').emit({ eventName:eventName, name:message.value() });
                    message.value('');
                });
                sandbox.service('socket').on(eventName, responseSocket.bind(self));
            },
            destroy:function(){}
        };

    }));
}( BEER ));

// var storage = sandbox.storage();
// storage.set('key', 'value');
// storage.getByKey('key');
//
// console.log(storage.getByKey('key'));
