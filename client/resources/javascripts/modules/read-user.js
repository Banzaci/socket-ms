(function(e){
    'use strict';
    e.CORE.subscribe('readUser', (function( sandbox ){
        var arr = [];
        var responseSocket = function(response){
            var self = this;
            var container = self.container;
            var outPutDiv = container.child('js-read-user');
            outPutDiv.html('');
            var df = sandbox.element(document.createDocumentFragment());
            response.forEach(function(user){
                df.createElement('li').html(user.name + ' ' + user.email + ' ' + user.age);
            });
            outPutDiv.append(df);
        };

        return {
            init:function(){
                sandbox.listen('newUser', responseSocket.bind(this));
            },
            destroy:function(){}
        };

    }));
}( BEER ));
