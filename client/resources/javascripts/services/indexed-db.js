(function( e ){
    'use strict';
    e.CORE.define('indexedDB', (function(){

        var indexedDB     = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
        var IDBKeyRange   = window.IDBKeyRange || window.webkitIDBKeyRange;
        var IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction;

        if (IDBTransaction) {
            IDBTransaction.READ_WRITE = IDBTransaction.READ_WRITE || 'readwrite';
            IDBTransaction.READ_ONLY = IDBTransaction.READ_ONLY || 'readonly';
        }

        function getAllUsers(transaction, storeName, cb) {
            var store = transaction.objectStore(storeName);
            var items = [];

            transaction.oncomplete = function(evt) {
                cb(items);
            };

            var cursorRequest = store.openCursor();

            cursorRequest.onerror = function(error) {
                console.log(error);
            };

            cursorRequest.onsuccess = function(evt) {
                var cursor = evt.target.result;
                if (cursor) {
                    items.push(cursor.value);
                    cursor.continue();
                }
            };
        }

        return {
          save:function( nameOfTable, data, cb ){
              if(indexedDB) {
                  //indexedDB.deleteDatabase('NameOfDB');
                  var request = indexedDB.open('NameOfDB', 2);
                  request.onupgradeneeded = function(e) {
                      var db = e.target.result;
                      var objectStore = db.createObjectStore(nameOfTable, { keyPath: "name" });
                      objectStore.createIndex("email", "email", { unique: true });
                      objectStore.createIndex("age", "age", { unique: false });
                  };
                  request.onerror = function(event) {
                      console.log('indexedDB error.');
                  };
                  request.onsuccess = function(event) {
                      var db          = event.target.result;
                      var transaction = db.transaction([nameOfTable], IDBTransaction.READ_WRITE);
                      var objectStore = transaction.objectStore(nameOfTable);
                      var add = objectStore.add(data);
                      add.onsuccess = function(e) {
                          cb(e);
                      };

                      add.onerror = function(e) {
                          cb(e);
                      };
                  };
              }
          },
          get:function(nameOfTable, cb){
              var request = indexedDB.open('NameOfDB', 2);
              request.onsuccess = function(event) {
                  var db = event.target.result;
                  var transaction = db.transaction([nameOfTable], IDBTransaction.READ_WRITE);
                  getAllUsers(transaction, nameOfTable, function (items) {
                      cb(items);
                  });
              };
          }
        };
    }));
}( BEER ));
//indexedDB.deleteDatabase('NameOfDB');
