(function( e ){
    'use strict';
    e.CORE.define('socket', (function(){
        var socket = (function() {
            return io.connect('http://localhost:5000')
                   .on('disconnect', function () {
                        console.log('- disconnected');
                    }).on('authenticated', function () {
                        console.log('- authenticated');
                    });
        }());

        var listenTo = [];

        return {
          emit:function( data ){
              if(io && socket){
                  socket.emit('notify', data );
              }
          },
          on:function( eventName, cb ){
              if(io && socket){
                  listenTo[eventName] = listenTo[eventName] || [];
                  listenTo[eventName].push(cb);
                  socket.on('listen' , function ( response ) {
                      if(response.eventName){
                          listenTo[response.eventName].forEach(function(cb){
                              cb(response);
                          });
                      }
                  });
              }
          }
        };
    }));
}( BEER ));
