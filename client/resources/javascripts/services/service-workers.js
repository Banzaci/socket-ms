(function( e ){
    'use strict';
    e.CORE.define('serviceWorkers', (function(){
        return {
          setUp:function( data ){
              if ('serviceWorker' in navigator) {
                  var sw = navigator.serviceWorker.register('/javascripts/service-worker.js').then(function(registration) {
                      console.log('ServiceWorker registration successful with scope: ',    registration.scope);
                  }).catch(function(err) {
                      console.log('ServiceWorker registration failed: ', err);
                  });
              }
              return false;
          }
        };
    }));
}( BEER ));
