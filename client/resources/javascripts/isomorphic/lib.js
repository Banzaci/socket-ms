var isNode = (typeof module === 'object' && module.exports);

(function(lib) {
	"use strict";
	var url = (isNode ? require('url') : null);
	lib.URLparse = function(str) {

		if (isNode) {
			   return url.parse(str);
		} else {
			url = document.createElement('a');
			url.href = str;
			return url;
		}

	};

})(isNode ? module.exports : this.lib = {});
