var io 				= require('socket.io-client');
var socket 		= io.connect('http://localhost:5000');
var Message 	= 'message';
var funcs			= [Message];
var i 				= 0;

socket.on(Message, function(data, fn){
    socket.emit('client', { message:i.toString(), eventName:data.eventName }, fn);
    i++;
});

socket.on('connect', function(){
    console.log("connect: Counter");
    socket.emit('func', { funcs:funcs });
});

socket.on('disconnect', function(){
    console.log('disconnect: Counter' );
});
