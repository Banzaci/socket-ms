var io 				= require('socket.io-client');
var socket 		= io.connect('http://localhost:5000');
var Message 	= 'message';
var funcs			= [Message];

socket.on(Message, function( data ){
    socket.emit('client', { message:data.name, eventName:data.eventName });
});

socket.on('connect', function(){
    console.log("connect: Message");
    socket.emit('func', { funcs:funcs });
});

socket.on('disconnect', function(){
    console.log('disconnect: Message' );
});
