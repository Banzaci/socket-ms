
var express         = require('express');
var app             = module.exports = express();
var io              = require('socket.io');
var port            = 5000;
var ms              = io.listen(app.listen(port));
var servers         = {};
var counter         = 0;
var dataToSend      = [];

ms.on('connection', function(socket) {

    socket.on('func', function(data){

        data.funcs.forEach(function(func){
            servers[func] = servers[func] || [];
            servers[func].push(socket);
        });
        console.info('New client connected (id=' + socket.id + ').');
    });

    socket.on('notify', function(data) {
        try {
          var eventName = data.eventName.trim();
          if(eventName) {
              servers[eventName].forEach(function(server){
                  server.emit( eventName, data );
              });
          }
        } catch(e){
            console.log(e.message);
        }
    });

    function emitToClient(data){
        counter = 0;
        ms.emit('listen', { eventName:data.eventName, message:dataToSend });
        dataToSend = [];
    }

    socket.on('client', function(data, fn) {
        var callLength = servers[data.eventName].length;
        dataToSend.push(data.message);
        counter++;
        if(callLength === counter){
            emitToClient(data);
        }
    });

    socket.on('disconnect', function() {
        var index,
            server,
            key;

        for(server in servers){
            if(servers.hasOwnProperty(server)){
                key = servers[server];
                key.forEach(function(s){
                    index = key.indexOf(s);
                    if(s.id === socket.id && index !== -1){
                        console.log( 'Disconnect on master: ', socket.id );
                        servers[server].splice(index, 1);
                    }
                });
            }
        }
    });
});

ms.set("transports", ['websocket', 'polling', 'htmlfile' ]);
